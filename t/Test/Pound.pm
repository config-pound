package Test::Pound;
use strict;
use warnings;
use parent 'Config::Pound';
use File::Basename;
use File::Temp;

sub new {
    my $class = shift;

    my $file = new File::Temp;
    while (<main::DATA>) {
	print $file $_;
    }
    $file->flush;
    my $self = $class->SUPER::new($file->filename)->parse;
    $self->{_file} = $file;
    return $self
}

sub file { shift->{_file} }

sub content {
    my $self = shift;
    my $fh = $self->file;
    seek $fh, 0, 0;
    local $/;
    return <$fh>
}

1;

    
    

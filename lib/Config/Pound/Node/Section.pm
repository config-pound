package Config::Pound::Node::Section;
use strict;
use warnings;
use parent 'Config::HAProxy::Node::Section';
use Carp;

my %match = (
    name => {
	wantarg => 1,
	matcher => sub {
	    my ($node, $value) = @_;
	    return $node->kw && lc($node->kw) eq lc($value);
	}
    },
    arg => {
	wantarg => 1,
	matcher => sub {
	    my ($node, $value) = @_;
	    my $arg = $node->arg($value->{n});
	    return $arg && $arg eq $value->{v};
	}
    },
    section => {
	matcher => sub {
	    my $node = shift;
	    return $node->is_section;
	}
    },
    statement => {
	matcher => sub {
	    my $node = shift;
	    return $node->is_statement;
	}
    },
    comment => {
	matcher => sub {
	    my $node = shift;
	    return $node->is_comment;
	}
    }
);

sub select {
    my $self = shift;
    my @prog;
    while (my $p = shift) {
	my $arg = shift or croak "missing argument";
	my $m = $match{$p} or croak "unknown matcher: $p";
	if ($m->{wantarg}) {
	    push @prog, [ $m->{matcher}, $arg ];
	} elsif ($arg) {
	    push @prog, $m->{matcher};
	}
    }
    grep { _test_node($_, @prog) } $self->tree;
}

sub _test_node {
    my $node = shift;
    foreach my $f (@_) {
	if (ref($f) eq 'ARRAY') {
	    return 0 unless &{$f->[0]}($node, $f->[1]);
	} else {
	    return 0 unless &{$f}($node);
	}
    }
    return 1;
}

1;

package Config::Pound;
use strict;
use warnings;
use parent 'Config::HAProxy';
use Config::Pound::Node::Root;
use Config::Pound::Node::Section;
use Config::Pound::Node::Comment;
use Config::Pound::Node::Statement;
use Config::Pound::Node::Empty;
use Text::Locus;
use Text::ParseWords;
use Carp;
use Data::Dumper;

our $VERSION = '0.1';

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(shift // '/etc/pound.cfg');
    $self->lint(command => 'pound -c -f');
    return $self;
}

sub reset {
    my $self = shift;
    $self->{_stack} = [ new Config::Pound::Node::Root() ];
}

sub dequote {
    my ($self, $text) = @_;
    my $q = ($text =~ s{^"(.*)"$}{$1});
    if ($q) {
	$text =~ s{\\(.)}{$1}g;
    }
    if (wantarray) {
	return ($text, $q)
    } else {
	return $text;
    }
}

use constant {
    PARSER_OK => 0,
    PARSER_END => 1
};

sub _parser_End {
    my ($self, $parent, $kw, $words, $orig, $locus, $fh, $ptab) = @_;
    $parent->append_node(
	new Config::Pound::Node::Statement(kw => $kw,
					   argv => $words,
					   orig => $orig,
					   locus => $locus));
    return (PARSER_END, $locus);
}

my %generic_section = ( 'end' => \&_parser_End );

my %match_section;

%match_section = (
    'match' => \%match_section,
    'not' => \&_parser_Not,
    'end' => \&_parser_End
);

my %rewrite_section = (
    'else' => \&_parser_Else,
    'rewrite' => \&_parser_Rewrite,
    'match' => \%match_section,
    'not' => \&_parser_Not,
    'end' => \&_parser_End
);

my %service_section = (
    'backend' => \%generic_section,
    'match' => \%match_section,
    'not' => \&_parser_Not,
    'rewrite' => \&_parser_Rewrite,
    'session' => \%generic_section,
    'end' => \&_parser_End
);

my %http_section = (
    'service' => \%service_section,
    'end' => \&_parser_End
);
	
my %top_section = (
# FIXME: If Include is to be expanded, this line should appear in all
# sections (since pound commit 6c7258cb2e).
#    'include' => \&_parser_Include,
    'listenhttp' => \%http_section,
    'listenhttps' => \%http_section,
    'acl' => \&_parser_ACL,
    'service' => \%service_section,
);

sub _parser_section {
    my ($self, $parent, $kw, $words, $orig, $locus, $fh, $ptab) = @_;
    # FIXME: extract label from $words etc
    my $section = Config::Pound::Node::Section->new (kw => $kw,
						     argv => $words,
						     orig => $orig,
						     locus => $locus);
    my ($r, $e) = $self->_parser($section, $locus, $fh, $ptab);
    $parent->append_node($section);
    return ($r, $e)
}

sub _parser {
    my ($self, $parent, $start_locus, $fh, $ptab) = @_;
    my $locus = $start_locus->clone;
    my ($filename) = $locus->filenames;
    while (<$fh>) {
	$locus->fixup_lines($filename, 1);
	chomp;
	my $orig = $_;
	s/^\s+//;
	s/\s+$//;

	if ($_ eq "") {
	    $parent->append_node(
		new Config::Pound::Node::Empty(orig => $orig,
					       locus => $locus));
	    next;
	}
	    
	if (/^#.*/) {
	    $parent->append_node(
		new Config::Pound::Node::Comment(orig => $orig,
						 locus => $locus));
	    next;
	}
	
	my @words = parse_line('\s+', 1, $_);
        my $kw = shift @words;
	if (my $meth = $ptab->{lc($kw)}) {
	    my ($r, $e);
	    
	    if (ref($meth) eq 'CODE') {
		($r, $e) = $self->${ \$meth } ($parent, $kw, \@words, $orig, $locus, $fh, $ptab);
	    } elsif (ref($meth) eq 'HASH') {
		($r, $e) = $self->_parser_section($parent, $kw, \@words, $orig, $locus, $fh, $meth);
	    } else {
		croak "Unsupported element type: " . ref($meth);
	    }
	    return (PARSER_OK, $e) if $r == PARSER_END;
	    $locus = $e;
	} else {
	    $parent->append_node(
		new Config::Pound::Node::Statement(kw => $kw,
						   argv => \@words,
						   orig => $orig,
						   locus => $locus));
	}
    }
    if (exists($ptab->{end})) {
	croak "End statement missing in statement started at $start_locus".Dumper([$ptab])
    }
    return (PARSER_OK, $locus);
}

sub _parser_Include {
    my ($self, $parent, $kw, $words, $orig, $start_locus, $fh, $ptab) = @_;
    my $filename = $words->[0] or
	croak "$start_locus: Filename is missing";

    # FIXME: Make sure filename is quoted
    $filename = $self->dequote($filename);
    
    open(my $ifh, "<", $filename) or
	croak "can't open $filename: $!";

    my $stmt = new Config::Pound::Node::Statement(kw => $kw,
					          argv => $words,
						  orig => $orig,
						  locus => $start_locus);
    
    my ($r) = $self->_parser($stmt, $start_locus, $ifh, $ptab);
    close($ifh);
    $parent->append_node($stmt);
    return ($r, $start_locus);
}

sub _parser_ACL {
    my ($self, $parent, $kw, $words, $orig, $start_locus, $fh, $ptab) = @_;
    # FIXME: Check $words
    my $locus = $start_locus->clone;
    my ($filename) = $locus->filenames;
    my @ip;
    while (<$fh>) {
	$locus->fixup_lines($filename, 1);
	chomp;
	s/^\s+//;
	s/\s+$//;
	# FIXME: Comments and empty lines are lost
	s/#.*//;
	next if $_ eq "";
	my @words = parse_line('\s+', 1, $_);
	if (@words > 1) {
	    croak "$locus: malformed ACL entry";
	}
	if (lc($words[0]) eq 'end') {
	    $parent->append_node(
		new Config::Pound::Node::Statement(kw => $kw,
						   argv => \@ip,
						   orig => $orig,
						   locus => $start_locus + $locus));
	    return (PARSER_OK, $locus);
	}
	    
	push @ip, @words;
    }

    croak "missing End in ACL statement started at $start_locus";
}   

sub _parser_Not {
    my ($self, $parent, $kw, $words, $orig, $start_locus, $fh, $ptab) = @_;

    $orig =~ s/^(\s*$kw)//;
    
    my $sec = new Config::Pound::Node::Section(kw => $kw,
				 	       orig => $1,
					       locus => $start_locus);
    my ($stmt, $e);
    if (@{$words} == 0) {
	croak "$start_locus: \"Not\" statement missing arguments";
    } else {
	$kw = shift @{$words};
	if ($kw =~ /^(match|not)$/i) {
	    (undef, $e) = $self->_parser_section($sec, $kw, $words, $orig, $start_locus, $fh, $ptab);
	} else {
	    $stmt = new Config::Pound::Node::Section(kw => $kw,
						     argv => $words,
						     orig => $orig,
						     locus => $start_locus);
	    $sec->append_node($stmt);
	    $e = $start_locus;
	}
    }
    $parent->append_node($sec);
    return (PARSER_OK, $e) 
}

sub _parser_Else {
    my ($self, $parent, $kw, $words, $orig, $start_locus, $fh, $ptab) = @_;
    my $section = new Config::Pound::Node::Section(kw => $kw,
						   argv => $words,
						   orig => $orig,
						   locus => $start_locus);
    $parent->append_node($section);
    return (PARSER_OK, $start_locus);
}

sub _parser_Rewrite {
    my ($self, $parent, $kw, $words, $orig, $start_locus, $fh, $ptab) = @_;
    my ($r, $e) = $self->_parser_section($parent, $kw, $words, $orig,
				         $start_locus, $fh, \%rewrite_section);
    my $rwr = $parent->tree(-1);
    my $itr = $rwr->iterator(inorder => 1, recursive => 0);
    my $branch;
    while (defined(my $node = $itr->next)) {
	if (lc($node->kw) eq 'else') {
	    $branch = $node;
	} elsif (lc($node->kw) eq 'end') {
	    last;
	} elsif ($branch) {
	    $branch->append_node($node);
	    $node->drop(); # FIXME: this requires modified Config::HAProxy
	}
    }
    return ($r, $e);
}

sub parse {
    my $self = shift;

    open(my $fh, '<', $self->filename)
	or croak "can't open ".$self->filename.": $!";

    my $locus = new Text::Locus($self->filename, 1);
    $self->reset();
    $self->_parser($self->tree, $locus, $fh, \%top_section);
    return $self
}    

sub write {
    my $self = shift;
    my $file = shift;
    my $fh;

    if (ref($file) eq 'GLOB') {
	$fh = $file;
    } else {
	open($fh, '>', $file) or croak "can't open $file: $!";
    }

    local %_ = @_;
    my $itr = $self->iterator(inorder => 1);

    my @rws = ([-1, 0]);
    while (defined(my $node = $itr->next)) {
	my $s = $node->as_string;
	if ($_{indent}) {
	    if ($node->is_comment) {
		if ($_{reindent_comments}) {
		    my $indent = ' ' x ($_{indent} * $node->depth);
		    $s =~ s/^\s+//;
		    $s = $indent . $s;
		}
	    } else {
#		print STDERR "# ".$node->as_string . "; depth ".$node->depth."; correction ", $rws[-1]->[1] . "\n";
		my $depth = $node->depth - $rws[-1]->[1];
		if ($node->is_section) {
		    if (lc($node->kw) eq 'rewrite') {
			push @rws, [ 0, $rws[-1]->[1] ];
		    } elsif (lc($node->kw) eq 'else') {
			if ($rws[-1]->[0] == 0) {
			    $rws[-1]->[0] = 1;
			    $rws[-1]->[1]++;
			    $depth--;
			}
		    }		
		} elsif ($node->is_statement && lc($node->kw) eq 'end') {
		    if ($node->parent &&
			$node->parent->is_section &&
			$node->parent->kw &&
			lc($node->parent->kw) eq 'rewrite') {
			pop @rws;
		    } else {
			if ($node->parent &&
			    $node->parent->parent && 
#			    $node->parent->parent->is_section &&
			    $node->parent->parent->kw &&
			    lc($node->parent->parent->kw) eq 'not') {
			    pop @rws;
			    $depth = $node->parent->parent->depth - $rws[-1]->[1];
			} else {
			    $depth--;
	                }		    
		    }
		}
			
		my $indent = ' ' x ($_{indent} * $depth);
		if ($_{tabstop}) {
		    $s = $indent . $node->kw;
		    for (my $i = 0; my $arg = $node->arg($i); $i++) {
			my $off = 1;
			if ($i < @{$_{tabstop}}) {
			    if (($off = $_{tabstop}[$i] - length($s)) <= 0) {
				$off = 1;
			    }
			}
			$s .= (' ' x $off) . $arg;
		    }
		} else {
		    $s =~ s/^\s+//;
		    $s = $indent . $s;
		}
	    }
	}
	print $fh $s;
	if ($node->is_section && $node->kw =~ /^not$/i) {
	    push @rws, [ undef, $rws[-1]->[1] + 1 ];
	    $node = $itr->next;
	    ($s = $node->as_string) =~ s/^\s+//;
	    print $fh " $s";
	}
	print $fh "\n";
    }

    close $fh unless ref($file) eq 'GLOB';
}


